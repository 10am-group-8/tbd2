# AdoptAPet

## Authors

* Sean-Matthew Aujong, sha567, seanaujong

* Tushar Kohli, tk22994, tkohli

* Nandu Vudumula, nrv379, nandukrv

* Edward Chamblee, eic348, edwardchamblee

* Sohum Kapoor, sjk2667, sohumk

## Git SHA

5352fbdb51eca196175b5fa0b40263d8cc1b0f50

## Project Leader

### Responsibilities

* schedule meetings
* track progress on feature branches
* monitor website health
    * NameCheap
    * Amplify
    * Elastic Beanstalk
    * RDS
* review merge requests
* Phase 1: Lead deployment
* Phase 2: Lead backend
* Phase 3: Lead frontend
* Phase 4: Lead frontend, presentation

### Phase Leaders

* Phase 1: Sean-Matthew Aujong
* Phase 2: Tushar Kohli
* Phase 3: Nandu Vudumula
* Phase 4: Edward Chamblee

## GitLab Pipelines

https://gitlab.com/10am-group-8/adopt-a-pet/-/pipelines

## Website Link

https://adoptapet.me

## Estimated Completion Time for Each Member

### Phase 1

* Sean-Matthew Aujong: 8 hours

* Tushar Kohli: 8 hours

* Nandu Vudumula: 8 hours

* Edward Chamblee: 8 hours

* Sohum Kapoor: 8 hours

### Phase 2

* Sean-Matthew Aujong: 16 hours

* Tushar Kohli: 16 hours

* Nandu Vudumula: 16 hours

* Edward Chamblee: 16 hours

* Sohum Kapoor: 16 hours

### Phase 3

* Sean-Matthew Aujong: 16 hours

* Tushar Kohli: 20 hours

* Nandu Vudumula: 20 hours

* Edward Chamblee: 13 hours

* Sohum Kapoor: 16 hours

### Phase 4

* Sean-Matthew Aujong: 8 hours

* Tushar Kohli: 8 hours

* Nandu Vudumula: 8 hours

* Edward Chamblee: 8 hours

* Sohum Kapoor: 8 hours

## Actual Completion Time for Each Member

### Phase 1

* Sean-Matthew Aujong: 10 hours

* Tushar Kohli: 9 hours

* Nandu Vudumula: 13 hours

* Edward Chamblee: 12 hours

* Sohum Kapoor: 12 hours

### Phase 2

* Sean-Matthew Aujong: 17 hours

* Tushar Kohli: 19 hours

* Nandu Vudumula: 20 hours

* Edward Chamblee: 18 hours

* Sohum Kapoor: 16 hours

### Phase 3

* Sean-Matthew Aujong: 16 hours

* Tushar Kohli: 20 hours

* Nandu Vudumula: 20 hours

* Edward Chamblee: 13 hours

* Sohum Kapoor: 16 hours

### Phase 4

* Sean-Matthew Aujong: 9 hours

* Tushar Kohli: 8 hours

* Nandu Vudumula: 9 hours

* Edward Chamblee: 8 hours

* Sohum Kapoor: 8 hours

## Comments

Thanks to Caitlin and Jefferson for being such great TA's!

## Deployment Issues

While the code seems to work locally, there appears to be some deployment issues with Elastic Beanstalk. Currently unsure how to diagnose. Sorry about that!
